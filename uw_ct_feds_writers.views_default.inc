<?php

/**
 * @file
 * uw_ct_feds_writers.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_writers_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_writer_profile';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds writer profile';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds Writer Profile';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Authors Photo */
  $handler->display->display_options['fields']['field_author_image']['id'] = 'field_author_image';
  $handler->display->display_options['fields']['field_author_image']['table'] = 'field_data_field_author_image';
  $handler->display->display_options['fields']['field_author_image']['field'] = 'field_author_image';
  $handler->display->display_options['fields']['field_author_image']['label'] = '';
  $handler->display->display_options['fields']['field_author_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_author_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_author_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Authors Name */
  $handler->display->display_options['fields']['field_authors_name']['id'] = 'field_authors_name';
  $handler->display->display_options['fields']['field_authors_name']['table'] = 'field_data_field_authors_name';
  $handler->display->display_options['fields']['field_authors_name']['field'] = 'field_authors_name';
  $handler->display->display_options['fields']['field_authors_name']['label'] = '';
  $handler->display->display_options['fields']['field_authors_name']['element_label_colon'] = FALSE;
  /* Field: Content: Authors Title/Position */
  $handler->display->display_options['fields']['field_authors_title_position']['id'] = 'field_authors_title_position';
  $handler->display->display_options['fields']['field_authors_title_position']['table'] = 'field_data_field_authors_title_position';
  $handler->display->display_options['fields']['field_authors_title_position']['field'] = 'field_authors_title_position';
  $handler->display->display_options['fields']['field_authors_title_position']['label'] = '';
  $handler->display->display_options['fields']['field_authors_title_position']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_writers' => 'feds_writers',
  );

  /* Display: Block - Feds writer profile */
  $handler = $view->new_display('block', 'Block - Feds writer profile', 'block');
  $handler->display->display_options['display_description'] = 'Block view for Feds news writers';
  $translatables['feds_writer_profile'] = array(
    t('Master'),
    t('Feds Writer Profile'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Block - Feds writer profile'),
    t('Block view for Feds news writers'),
  );
  $export['feds_writer_profile'] = $view;

  return $export;
}
