<?php

/**
 * @file
 * uw_ct_feds_writers.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_writers_features_override_default_overrides() {
  // This code is only used for UI in features. Export alter hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.search feds_writers content.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.search feds_writers content.roles|authenticated user"] = 'authenticated user';

  // Exported overrides for: variable.
  $overrides["variable.rh_node_override_feds_writers.value"] = 1;

  return $overrides;
}
