<?php

/**
 * @file
 * uw_ct_feds_writers.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_writers_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'enter feds_writers revision log entry'.
  $permissions['enter feds_writers revision log entry'] = array(
    'name' => 'enter feds_writers revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_writers authored by option'.
  $permissions['override feds_writers authored by option'] = array(
    'name' => 'override feds_writers authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_writers authored on option'.
  $permissions['override feds_writers authored on option'] = array(
    'name' => 'override feds_writers authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_writers promote to front page option'.
  $permissions['override feds_writers promote to front page option'] = array(
    'name' => 'override feds_writers promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_writers published option'.
  $permissions['override feds_writers published option'] = array(
    'name' => 'override feds_writers published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_writers revision option'.
  $permissions['override feds_writers revision option'] = array(
    'name' => 'override feds_writers revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_writers sticky option'.
  $permissions['override feds_writers sticky option'] = array(
    'name' => 'override feds_writers sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_writers content'.
  $permissions['search feds_writers content'] = array(
    'name' => 'search feds_writers content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
